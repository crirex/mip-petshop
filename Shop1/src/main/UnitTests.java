/**
 * 
 */
package main;

import static org.junit.jupiter.api.Assertions.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;

import org.junit.jupiter.api.Test;

import util.DatabaseUtil;

/**
 * @author crirex
 *
 */
class UnitTests {

	/**
	 * We test if the singleton implemented works as expected.
	 *
	 */
	@Test
	void singletonTest() {
		DatabaseUtil database = DatabaseUtil.initialize();
		assertEquals(DatabaseUtil.initialize(), database, "Singleton must be the same");
	}
	
	/**
	 * The function verifies if the server returns what it's supposed to return.
	 *
	 */
	@Test
	void serverTest()
	{
		Socket socket;
		String response = null;
		try {
			socket = new Socket("localhost", 5003);

		BufferedReader echoes = new BufferedReader(new InputStreamReader(socket.getInputStream()));
		PrintWriter stringToEcho = new PrintWriter(socket.getOutputStream(), true);

		
		stringToEcho.println("username parola");
			response = echoes.readLine();
		} 
		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		assertEquals("good", response, "Server must return good");
	}

}
