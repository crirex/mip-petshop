package controllers;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import javax.imageio.ImageIO;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import main.Main;
import model.Animal;
import model.Programare;
import util.DatabaseUtil;

public class MainController implements Initializable {
	
	DatabaseUtil db = DatabaseUtil.initialize();
	Boolean uploadedFromDatabase = false;
	Map<String, Animal> animalMap = new HashMap<String, Animal>(); 
	Animal curentAnimal;
	
	public static Stage historyStage;
	public static List<Programare>  curentProgramari;
	public static int programariIndex;
	
	@FXML
	 private ListView<String> mainListView;
	@FXML
	 private Label nameLabel, ageLabel, specieLabel, weightLabel, ownerLabel, phoneLabel, emailLabel;
	@FXML
	 private ListView<String> listView, listHistory;
	@FXML
	 private Button addAppointment;
	@FXML
	 private ImageView photoView;
	@FXML
	void addAppoinmentClicked(ActionEvent e) {
		System.out.println("test");
	}
	
	@FXML
	void historyClicked()
	{
		curentProgramari = db.selectProgramari(curentAnimal.getName());
		programariIndex = listHistory.getSelectionModel().getSelectedIndex();
		try {
			historyStage = new Stage();
			BorderPane pane = (BorderPane) FXMLLoader.load(getClass().getResource("History.fxml"));
			Scene scene = new Scene(pane, 800, 800);
			scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
			historyStage.setScene(scene);
			historyStage.show();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public interface OnCompleteListener {
	    public void onComplete();
	}
	
	public class Populate implements Runnable {
	     
	    private OnCompleteListener onCompleteListener;
	     
	    public void setOnCompleteListener(OnCompleteListener onCompleteListener) {
	        this.onCompleteListener = onCompleteListener;
	    }
	     
	    @Override
	    public void run() {
	        try {
	        	db.setUp();
	    		db.startTransaction();
	    		List<Animal> animalDBList = (List<Animal>)db.readAnimal();
	    		for(Animal animal : animalDBList)
	    		{
	    			animalMap.put(animal.getName(), animal);
	    		}
	    		ObservableList<Animal> animalList = FXCollections.observableArrayList(animalDBList);
	    		listView.setItems(getAnimalNameObservableList(animalList));
	    		listView.refresh();
	            onCompleteListener.onComplete();
	        } catch (Exception e) {
	            System.out.print(e.getMessage());
	        }
	    }
	}
	
	void poza()
	{
		try {
            String dirName = "C:\\Users\\crire\\Desktop\\Shop1";
            BufferedImage imag = ImageIO.read(new ByteArrayInputStream(curentAnimal.getPhoto()));
            ImageIO.write(imag, "jpeg", new File(dirName, "img.jpeg"));
        } catch (Exception e) {
            System.out.println(e);
        }
        try {
        	//De schimbat in functie de ce ruleaza asta calculator 
            FileInputStream inputStream = new FileInputStream("C:\\Users\\crire\\Desktop\\Shop1\\img.jpeg");
            Image i = new Image(inputStream);
            photoView.setImage(i);
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
	}
	
	@FXML
	void animalClicked()
	{
		if(uploadedFromDatabase)
		{
			Main.logger.setLog("All animals aquired.");
			String name = listView.getSelectionModel().getSelectedItem().toString();
			Animal animal = animalMap.get(name);
			curentAnimal = animal;
			nameLabel.setText(animal.getName());
			ageLabel.setText(new Integer(animal.getAge()).toString());
			specieLabel.setText(animal.getSpecie());
			weightLabel.setText(new Integer(animal.getWeight()).toString());
			ownerLabel.setText(animal.getOwner());
			phoneLabel.setText(animal.getPhone());
			emailLabel.setText(animal.getEmail());
			poza();
			listHistory.setItems(getProgramariHistoryObservableList(name));
			listHistory.refresh();
		}		
	}
	
	public void populateMainListView() {
		Populate populate = new Populate();
		populate.setOnCompleteListener(new OnCompleteListener() {
            @Override
            public void onComplete() {
                uploadedFromDatabase = true;
            }
        });
		populate.run();
	}
	
	public ObservableList<String> getAnimalNameObservableList(List<Animal> animals) {
		ObservableList<String> programari = FXCollections.observableArrayList();
		for (Animal a: animals) {
			programari.add(a.getName());
		}
		return programari;
	}

	public ObservableList<String> getProgramariHistoryObservableList(String name)
	{
		ObservableList<String> lista = FXCollections.observableArrayList();
		List<Programare> programari = db.selectProgramari(name);
		for(Programare prog : programari)
		{
			lista.add(prog.getData().toString() + " - " + prog.getLocation() + " - " + prog.getPersonalmedical().getName() + " - " + prog.getTitlu());
		}
		return lista;
	}
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		populateMainListView();
		
	}
}
