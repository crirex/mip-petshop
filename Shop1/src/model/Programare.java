package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the programare database table.
 * 
 */
@Entity
@NamedQuery(name="Programare.findAll", query="SELECT p FROM Programare p")
public class Programare implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int idProgramare;

	@Temporal(TemporalType.DATE)
	private Date data;

	private String descriere;

	private String location;

	private String titlu;

	//bi-directional many-to-one association to Animal
	@ManyToOne
	@JoinColumn(name="idAnimal")
	private Animal animal;

	//bi-directional many-to-one association to Personalmedical
	@ManyToOne
	@JoinColumn(name="idPersonalMedical")
	private Personalmedical personalmedical;

	public Programare() {
	}

	public int getIdProgramare() {
		return this.idProgramare;
	}

	public void setIdProgramare(int idProgramare) {
		this.idProgramare = idProgramare;
	}

	public Date getData() {
		return this.data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public String getDescriere() {
		return this.descriere;
	}

	public void setDescriere(String descriere) {
		this.descriere = descriere;
	}

	public String getLocation() {
		return this.location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getTitlu() {
		return this.titlu;
	}

	public void setTitlu(String titlu) {
		this.titlu = titlu;
	}

	public Animal getAnimal() {
		return this.animal;
	}

	public void setAnimal(Animal animal) {
		this.animal = animal;
	}

	public Personalmedical getPersonalmedical() {
		return this.personalmedical;
	}

	public void setPersonalmedical(Personalmedical personalmedical) {
		this.personalmedical = personalmedical;
	}

}