package controllers;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextArea;
import javafx.scene.text.Text;
import model.Programare;
import util.DatabaseUtil;

public class HistoryController implements Initializable {
	
	DatabaseUtil db = DatabaseUtil.initialize();
	
	@FXML
	void onClose()
	{
		MainController.historyStage.close();
	}
	
	@FXML
	private TextArea descriptionText;
	
	@FXML
	private Text titleText;
	
	public void populate() {
		Programare programare = MainController.curentProgramari.get(MainController.programariIndex);
		titleText.setText(programare.getTitlu());
		descriptionText.setText(programare.getDescriere());
	}
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		populate();
		
	}
}