package controllers;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import main.Main;
import util.LoggerAgent;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;

public class LoginController implements Initializable {
	
	@FXML
	private Button login;
	@FXML
	private TextField username;
	@FXML
	private TextField parola;
	
	public static Stage mainStage;
	
	boolean getAnswerFromServerForLogin(String string)
	{
	try {
			Socket socket = new Socket("localhost", 5003);
			BufferedReader echoes = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			PrintWriter stringToEcho = new PrintWriter(socket.getOutputStream(), true);

			String response;
			stringToEcho.println(string);
				response = echoes.readLine();
				if(response.equals("good"))
				{
					socket.close();
					return true;
				}
			socket.close();
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	return false;
	}
	@FXML
	void loginClicked()
	{
		Main.logger.setLog("Login clicked");
		String usernameOnClick = username.getText().toString();
		String parolaOnClick = parola.getText().toString();
		if(getAnswerFromServerForLogin(usernameOnClick + " " + parolaOnClick))
		{
			try {
				mainStage = new Stage();
				BorderPane pane = (BorderPane) FXMLLoader.load(getClass().getResource("MainView.fxml"));
				Scene scene = new Scene(pane, 800, 800);
				scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
				mainStage.setScene(scene);
				mainStage.show();
				Stage curentStage = (Stage)login.getScene().getWindow();
				curentStage.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		//Empty
	}
}
