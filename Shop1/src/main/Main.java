package main;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import util.LoggerAgent;
import util.LoggerChannel;


public class Main extends Application {
	
	public static LoggerAgent logger;
	@Override
	public void start(Stage primaryStage) {
		try {
			logger.setLog("Main has launched");
			BorderPane root = (BorderPane) FXMLLoader.load(getClass().getResource("/controllers/Login.fxml"));
			Scene scene = new Scene(root, 800, 800);
			scene.getStylesheets().add(getClass().getResource("/controllers/application.css").toExternalForm());
			primaryStage.setScene(scene);
			primaryStage.show();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		logger = new LoggerAgent<String>();
		logger.addPropertyChangeListener(new LoggerChannel<String>());
		launch(args);
	}
}
