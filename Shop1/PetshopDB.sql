CREATE DATABASE  IF NOT EXISTS `petshop` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */;
USE `petshop`;
-- MySQL dump 10.13  Distrib 8.0.13, for Win64 (x86_64)
--
-- Host: localhost    Database: petshop
-- ------------------------------------------------------
-- Server version	8.0.13

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `animal`
--

DROP TABLE IF EXISTS `animal`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `animal` (
  `idAnimal` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `age` int(11) DEFAULT NULL,
  `specie` varchar(45) DEFAULT NULL,
  `weight` int(11) DEFAULT NULL,
  `owner` varchar(45) DEFAULT NULL,
  `phone` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `photo` blob,
  PRIMARY KEY (`idAnimal`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `animal`
--

LOCK TABLES `animal` WRITE;
/*!40000 ALTER TABLE `animal` DISABLE KEYS */;
INSERT INTO `animal` VALUES (1,'Biberi',3,'Caine',20,'Cineva','0000000000','email@email.com',NULL),(2,'Pika',4,'Monstru',5,'Ash','7653263463','t@t.wow',NULL),(3,'YAll',5,'Papagal',1,'Leila','1212343456','leila@gmail.com',NULL),(4,'Kos',12,'Nu',1000,'Nimeni','0707700077','kos@kosm.kos',_binary '�\��\�\0JFIF\0\0\0\0\0\0�\�\0�\0	( %!1!%)+...383-7(-.+\n\n\n\r\Z\Z-%-----------+-----------------------+--------------��\0\0�,\"\0�\�\0\0\0\0\0\0\0\0\0\0\0\0\0\0�\�\0=\0\0\0\0\0!1AQa\"q���2�\��R�Br�#�\�\�3Sb����\�\0\0\0\0\0\0\0\0\0\0\0\0�\�\0\'\0\0\0\0\0\0\0!1AQ\"Ra��ѱ2�\�\0\0\0?\0�~\"&B�z�8\0�\ntB\�\�#�w��v)�\���:iq�5.œ\Z�?\���ϟ\�\�\�|�\0\��9\�,\�)�73�\0Χ\�M�H�hN�\�\�9�\�\�~S��\0\�2ߛ#9��\�\�\�H-\ZC$:d���z�\�\��H\�o~\�	�O\��3R��\��/I���D�X�\�\�u\0n\"$x��\�7�Y�O}�N�����\�\��F�\�\��D\�N&�G\�\�e\�L�*?\n$ ���k`\�	̼�B\�/Ue\�5Q��[(�I�\Z��\�6M��\�F���<�:F�\0��\�S\�x� ��:\�+O�\�Ϥe3�\� F� �\�\'�\��\�j\�5>\�rG0�\�(�a\�M�J�\�.\�7Vq\�\�\�t�	x$�v�\�{X\�� AԐ*SS~8rJ�\'\��TЬb�b5Q\rc�w\��7o\�\�$=P75�79�q\�\�\0B%?e��\�4�\�3�Z&w���񍺮b�7L�uCm\�\�Wqǚ���m<;C\'�:�\�^�\�㛸\�$�j\r\"Ǽt�#�ո�\�5��L�\�\�^f\0�%J�(v�ĵ���\0O\�\�R\�\r��\'�\�\'\�x^T|�\�3\�!\�\�\�\�\�\�\��\0\�|J���i�Cb�)���X#�L��\�BB�\��Y&O�z\�\0����\�N�\�oxVM&Yr�%V�\�{N\�U�\\;c�$�\0\Z8S\"\�\�\�(�Mb\��\0,�v��\0{�=�Z\\\'\�l�\�o���#\0ܹ\�i�*\�c� �9\��\�,56\0Cc\�\�3\�1\�sHo�\�\�&�>CS\r\�^�.����yKI�Zd�)\�d���H�]0�� !׮\Z\'\��Uv0\�&\�=��NXC\�w�ok	\�6\�w�\Z��\�.�î�����n]��N��	x$H3m$n<R\�u�<#W��ʉ�x�8���\�+j��ǒ��1R\�*mm��y�D�����Ts@uQ\�	 �\�Dx�N\�sX\�2�G�\�h�\�\Zd�`u�\n�Uꖬ�&貽S��5?���� �j@�$k0&\�	\\qPv\�\�\�\�\�֍E���\\\", �	�\���m6\��D�@�g2\r0)��ec\nb��wM\�c��;z�\�\\�+�\�Y��ƁyH5\�\�,i\�7J�橘�\�\�44�v\�pknN�k0�m9���j 7�����S.t���>����\��<W�\���p@.�Dm\'�X����9��P���\�\�s\�h���jۤ,\��\�K��Z\��\�X֢\ZC��ۈY�=Xx=\�S�\�\�\�b\�+;�P,c��{Э��:Zy��\�_&\Z����P5�[�Xz\�\n�\�N\�\�\�� ����\�\�\0Q\�a\�&\�^B\�+\�qQ���{\�4I߀��\��\Z�	k�e��:4O�,\�\�&���sA\�b8n}��~[�CHl\�o\r�֋?)�[\Zv�C��\�-�w�mg���d�\�\n\�\��7�܋�/e ��\�q��W�\�lr<֓��UE�&#bv#��Y�doy���28s�.U�o�\�vu\�\rU\��\�,J\�\�c�*&�J9ǑQ\�:\�qՐ\�D��.�\�@�\�z�\�q\�P�\��z(v�Te\�\�{n�\�PR\�}�H�\�\�_�	��P%A�ʁ�%\0\�� P\0\��So����������-�\�T\�\r(�@\�\�7B�U^\�#SpE\�I�\��\�\�a�\�phm�\�e<?}\�c�����\�itKL�<\�h\�{��C�S��}g���C x�\�\�K��r���Ŗ\�c^t�3i\�	��4:Hs5\r�Xʒ58H\r�\�s\�_|1�5�\�ÊO\\��\�w�-�WY[\�jdD�D&���\0��\� �%�j�@\� \��\�7\�\0\�\�:`\'��Ď�w}\�e,m\���HT�\�\�Л\���\�N˘\�iW&�\�+1�b\�\\	����\�(\�ꣻ�0\��@̝�\��N\�X60\�u�&�\�>2�$��y[\�\�p\0\�.q$_kO�\�1�\�Y/q�\�11k�\'�͠�Dj�#�k\�D�\�~�+�4Ü-:O\�41ނ:]j05�\Z#I��z\�Sf�y~\���ZU5Z\�(\�\�ͺ&W��\��տ06�\�@�\��\�**�\��W�\0ዏh.�w�+:\�Q\��\�\"\�<��\�\��pUv\�H�d���Ǟg\�jmg��\�\��7O!�\�?��(q��\�\�%y��\�+��n�N��O\�F�\�\�\�ݧ\��<\�\��g�r���\�8���,\�G�{$\�H\�ei�?�\�u��B��ߨ\�:�o#\���tp=\� p�� jt�*.&~Q��AG_\�5\�\��>\�\�{�=\nq\�=�\0S<��*b�UDz.��*�\�(�=!Z\����J#Th\�K�\�\0LQ!�\��R\�\�F�A\�\�\����t��L4Xp���\�a�}�&��uS��\�f�7jw�Pߏ5C�\�d�\�\n\�\��\Z*�\�@�CA�F�\��\�7=�\��ܞ�4qo�;[8\�\��J\�d��-\�\�� \�ᔯ�==..�\'X�0n��YH�=\��\�e]��\�t\\\�\�ˢU�s_m\�&w\�7��\�.Ʋ:�=\�U\�	<�\�|M�u*Ł\�4@(��vU�Ƃu}\\\�X;���\�}�\�[�jC[��4�\�w\"|w�K�\0{C_-\�\��V�\�kk�\0�R�i�G\�,N7i\�sL�$r�+X\�/��yk�\�d\�\�7wj9�P�\�ݕ���ݤ\�\��\�\��|��5 �\��\�:-o\�mkY��7#i�</$�ޛ\�vv�\�;P\�\�/\�ٕ\�\�\��:BOhf \\@� ���\�\��|��$\�\�\�\�F�\\>��\�$��G{\�\���F8�F�g���g���=Uf+nN��\�W\\gnY]�a�\r۫đ\0�����\�\��Q�~J\�ft=L\�\��S\�\�TO犠�¡GA\�\�\�i�Mq\��uM\�&\�{q�,\���w\��:������\Z\�\�bۉ{�X\0`�>)\\\�\��\�8�\0��B\�q\�L\�i���U�\�/�[q�1�G̱T�\�AlDb�!�P���卪.<���\�\�ņ�̈�\�\\s�9e��g\���+�#\�=R�H��A�r>K�%�$�>�W�$ro�\��Q\�@\�\����C:�4���EU\�}\'��OE\�@A\�r]�\�	P0Ҝ�\�/pl��4\�\�*\�*n���\��U�4;\�$j@�\Z\���:lѣ�\�`����\��\'�ڍ\���8����\�\�K@\'s+秷�\��\�+��\�4\\\Z\"5�$�-\rl\�S�L1\�\�3;XIS&\�M\�k˅0KA�\�.W~\Z�{�b\�\�bv�\�6�.46�uG9�Ӻ�����ߦᤑ n\�YW\�p�\�S\"��\�\�<F�\�Pg9\�s;6�\r\�y�\�\�\�f\��U�d�<\�2\�g{\"a�(�N�2@\�\�z��S��_iK��\�7��>vY[_E\�3�.�\0�\�&\�\�\0n[\�->1�kS�\�����\0�z��U\�\�h-&\�\�\"���\r&A\��\�B\�\�}�r\�M��qNL;�o\�P\�@��0�\�\�\��wK��-ӊ�\\cDVi-d\�fn&Hv�ȉ�c�\� \�p\�:���b<;z0�c3�\�4\�\�s�:ET\�_D\�)6�hsa�)���\�- \��\0e�\�\�,{�bZKO�0W|{�>x\�\�K�4\�O>	�\�\�\�><M��T\�\Z�E��uyV��@9�\�3\�\�t[\��S_rO��j׍\�\�АL�q�xz.\�Ŗ\��ߨ+V0�\�\nB�\�b�\�#t�/Y�$l/\�/���FG{�\\s�	�*��\0��\�\�\�\�pE��l��,�W6��Vy~j\�w�G)�*6��!1\�K6�\��f��\�\�rKc�6�=~�/�b��^Āxn�̥<\"��\�\�s�_�\�a1\�}\�g\n73�\�j�80D\�\�)�eu\�\�ʔ\�ŉ���Z!\�Zy����Ϥ-[RT���\�d�.�k\�\�m�\�\Z\���sA:g��^N\�\0��PJ�2�)\�Gf\�Q7s\�\�I�o\�\�UP\'�b�F\�\�w���cM�\�R^XL4\��Ϣ\�eX\�\�k\�Q�u=���z�VGKS���0\Z\�;��0x�t]R��>\���\�p\�\�_Kņ���e�4^\�\��\'�n�u\�Q�!Â\�	&Z\�\�\"\�Z,�\�ƣH\�\�` �\n�\�Sߥ�f�Ĵ\�%#��M�j\��˝wC�<\�pa&\��&\�\�bn\Z\���i�|9C\�$���X;qi����\�\���m�\�p�v��>I|z	Y��<��\�M�[�si���\�\�\�C�h$�\�\�\�ϳZ�t�\�\�k\n\���!l=}3i�vbn$lR����\�dH\�\'\�uy+AW{&�]�0؈3$��\�Yd\�\�h\��\'���\�2��\�\n��X�I�	h3\�\�\�)�\\.�Dɰ�\�s�8\�KZ\�0� ~\�	�NSR�W��\�$��\�\�t*�*�����\�\��M|C�Q\�\����D4���\�J\�;�3�(�����\0\�C������\�\�\'\�uM�\�k��9��=��R\�n\�\n\�sq�.�kj�}Z�{ZӢ�-;\��ς�g�G4��c��^Alq�C^�:\�����è�rYN\Zap|痊\�9r��<n�e��ڂLaÄˢ�Ը{��9\�\�[�_�\����A�\Z�\�\rm͹��!G�5^\\\�x�ӛ�\�R\06�y\'\�|x���\�\�=\�ഖ\��7���M�c�O嘭2\��\�p\�\�u	�Tƕ7��\�2�L�\�G;*�\'Wt\�\�\ZM<�\r��\�ʧ\�\�	 ��N\�\�\�	]�\��y�\��\�\�X\�\�\�\�P\�X\"\�^.��\�6!q�dB\�7+�ՌOo\�\�\�opV��K�ǒ��/�\�2���.�������q@RG潼�E.h��k�\"P7�\�\�\r�.\�\�~���8xW\�$�s\0u\�G��M\�\�\�փm.�.O�\�ey�7��x\�@\�\�7�Yj�\�a�\�\�h�	�Q�E\�\�v�\�e�\�H�.6�\� 6\rG\�\�<VD\�?UZ�K�R\0�\�H\�)\�x\�!\�k\�wHkv��\�\�1n�\�j���L[�d^<��8o\��1\������e\�\�c�4\�\"�s\�LΘ�υ�[>\�\�\�J�\\\�n6��v�o�\�9\�l\�@`\0��0	�����\�.S\�a���U�\�ц:|_/��[\Z�\��=q�8*�f\�*�\�]t�\�\�Ș\\k\�\�	ΐ \�\���o\�\�M�{��Q�\Zn��\�G�\�\�~*��\�@\�A\�K6K���c�>��NF�@<�7�=��\\\�q�\�߲��ip�~�H \�\�s�`�3x\�\�X_�L\�Hc\00Z\�\�7N\�sqP5\�Ti\�\�\��\'�곮�ev�R-�>\�q�ɡ��YX65���~�\�H\�\�n��\��7\�)z��1�u\�\�˴\�A\�8?T�D\�\�A�\�\�a\�\�\�\�F\�\��\0���H2\�\�O�_;�c\�H\�\�=\�\�+�/\�8�\�\�\��͗\�e�� ǀM:m\0�8񞜖{	�s��\�\0<\��i�\�\\�4	-w\�E�\ZDU�tM�\�L�x\�\0%]��\��j1��\�\�5U\�M�}\�\�6\�8��o!�#\�\rV\0��rWf�V9\���\�u��B�{-�&�sM�cp�8��\�\�K�u\�H��\��\ZG�\�X�j�hDNz\�B�1\�@)l�;\\�	\�-&h%\��\�d\��\0b�29r)�N_@��t�\0x\�[h\�\�\�\Zƍ�Si\�q�^0\�e��ԫL\n�@}7\���\�\0Y�\�S\�H�fw$^cU��^�	�ג54\�\�\�\�c)�\��g$\�\�\�Q�i$\�s�\�i3G�*��`.]ݎ\��;��\�����\�W�\0��U\����\�\��\������^a\�\�gy\�si�\�ك\�\��u�\�b����/W�\�f�\�7�U��J\�\Zx|�\0��\�{=R������*<�.�y�CT0\�\Z\�e*`�\�\�7�\���\��A���E��� �\0I3�Ɨ�\�â��H\�\�p8�6\��\'j8B5<(\"\�O��;\�aKW?�|Gf���i\��;\�\�\�y\"8M�P\�R{\�\�\"G\�L�\� ����]\�\�X�<R\�i��Vt\��\��*�2�|�\�\�h#qv�s��JWG\�@��x\"Ӯv\\�T��/�\�\�V�����M�\�x;H�4\Z?��cB�&\�6p�q\�ks\\+��e��p\��d�E�\�5Ĉ_C�|G\�2�\r\�\�\�\��/�jY\�Z�M��\�\�$�\0\�J�~s@�/h�\�\��[���N�\�$�<7Y\�5���\�\�2\� \�*\�@x7iLeu\��7i�\��\�lE`�\�n\�%`���5\�w�[SH\�FiA=K\���\�q\�k4\0Đ^	�Q�kw�^v-��c�����\n+\\�q\��Ʊ\�)\�F��\�\�\�[�\�\�*\�i�\�s�a�\�}U�*!�O��î?�\�{՞/2.q\�\�\�xK߷��s=,2%\��\��\0Z�i]{O��\�_6wޤ\�\�\�\��U�\0Z�-\����\�Zmp�;�����<��W�Yʡ��y9�\0uя��\0q�\0\�)9R� X�<x|D���W�fI�\�\��N��R>w����U\�[twnU~�/�?\��\0\"\���\�s\�q<K��\�*Xj �i ��:�(?\�\n\�pc\�jH\�\�P°�oRT݅e��\�w\�\�\��+�v�X�\�>�f%M�g/��g\�PA\�o\��\0u\�g\��\0tF�E\�B�m�#\�\���r�|g�*�P�U6;\�&�ǫ�\�&�w��\'՛��\�D?��\�}!�\0l	��X�$N\�O>�>\��DmBy)�jN\�\n\�	�\�\�<�\�\�ܡ��F�\n�U�D�-72x\��q\0�D�\�).�3\�[��?ت-�\�pf�z�p\�v�pɸ>�������a\�<\�EYr\���:&\�\�7�\�\�J�T���\":�A]^PM�\0���.�>\�./ 3j)5��\�	�^^Qvj�Z\�\�\�{\�H�/I)/\�G5\�\�ڥO5Ӄ\�ח�٤18mݪ\���Z�\�\�qJ{\0��\�u�^^R�\��\�]S�//(<\�7e\�\�yy\0ޠWW�!x5yyT@�6�;�/ )3\�\�+\�\�!\�%\�Qyy�\�9\�\�]\�yyU�\�');
/*!40000 ALTER TABLE `animal` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `personalmedical`
--

DROP TABLE IF EXISTS `personalmedical`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `personalmedical` (
  `idPersonalMedical` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `age` int(11) DEFAULT NULL,
  `sex` varchar(1) DEFAULT NULL,
  `inaltime` int(11) DEFAULT NULL,
  `greutate` int(11) DEFAULT NULL,
  PRIMARY KEY (`idPersonalMedical`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `personalmedical`
--

LOCK TABLES `personalmedical` WRITE;
/*!40000 ALTER TABLE `personalmedical` DISABLE KEYS */;
INSERT INTO `personalmedical` VALUES (26,'Jefa',26,'M',178,67),(27,'Jefra',21,'S',157,45),(30,'Jefa',54,'M',123,21);
/*!40000 ALTER TABLE `personalmedical` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `programare`
--

DROP TABLE IF EXISTS `programare`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `programare` (
  `idProgramare` int(11) NOT NULL,
  `data` date DEFAULT NULL,
  `idAnimal` int(11) DEFAULT NULL,
  `idPersonalMedical` int(11) DEFAULT NULL,
  `location` varchar(45) DEFAULT NULL,
  `titlu` varchar(45) DEFAULT NULL,
  `descriere` varchar(600) DEFAULT NULL,
  PRIMARY KEY (`idProgramare`),
  KEY `idAnimal_idx` (`idAnimal`),
  KEY `idPersonalMedical_idx` (`idPersonalMedical`),
  CONSTRAINT `idAnimal` FOREIGN KEY (`idAnimal`) REFERENCES `animal` (`idanimal`) ON DELETE SET NULL,
  CONSTRAINT `idPersonalMedical` FOREIGN KEY (`idPersonalMedical`) REFERENCES `personalmedical` (`idpersonalmedical`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `programare`
--

LOCK TABLES `programare` WRITE;
/*!40000 ALTER TABLE `programare` DISABLE KEYS */;
INSERT INTO `programare` VALUES (1,'1970-01-01',4,26,'Brasov','Programare','Nimic'),(2,'1970-01-02',4,27,'Brasov','Programare','Nimic'),(3,'1970-01-02',4,30,'Bucuresti','Programare','Nimic'),(4,'1970-01-02',1,26,'Brasov','Programare','Nimic'),(5,'1970-01-02',NULL,27,'Brasov','Programare','Nimic'),(6,'1970-01-02',3,27,'Bucureti','Programare','Nimic'),(7,'2018-01-23',3,30,'Brasov','Programare','Nimic');
/*!40000 ALTER TABLE `programare` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'petshop'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-11-15 23:48:50
