package util;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import model.Animal;
import model.Personalmedical;
import model.Programare;

public class DatabaseUtil {
	public static DatabaseUtil databaseUtil = null;
	public static EntityManagerFactory entityManagerFactory;
	public static EntityManager entityManager;
	
	private DatabaseUtil()
	{
		
	}
	
	public static DatabaseUtil initialize()
	{
		if(databaseUtil == null)
			databaseUtil = new DatabaseUtil();
		return databaseUtil;
	}
	
	/**
	 * Setting up the entityManager
	 */
	public void setUp(){
		new Persistence();
		entityManagerFactory = Persistence.createEntityManagerFactory("Shop");
		entityManager = entityManagerFactory.createEntityManager();
	}
	
	/**
	 * Begins the connection
	 */
	public void startTransaction() {
		entityManager.getTransaction().begin();
	}
	
	/**
	 * Flushes the changes
	 */
	public void commitTransaction() {
		entityManager.getTransaction().commit();
	}
	
	/**
	 * Closes the entityManager
	 */
	public void closeEntityManager() {
		entityManager.close();
	}
	
	/**
	 * A Create function for Animal in the database
	 */
	public void createAnimal(Animal animal) {
		startTransaction();
		entityManager.createNativeQuery("Insert into petshop.Animal(idAnimal, name)" 
		+ " Values (?, ?)")
		.setParameter(1, animal.getIdAnimal())
		.setParameter(2, animal.getName())
		.executeUpdate();
		commitTransaction();
	}
	
	/**
	 * An Update function for Animal in the database
	 */
	public void updateAnimal(Animal animal) {
		startTransaction();
		entityManager.createNativeQuery("Update petshop.Animal Set name = ? Where(idAnimal = ?)")
		.setParameter(1, animal.getName())
		.setParameter(2, animal.getIdAnimal())
		.executeUpdate();
		commitTransaction();
	}
	
	
	public Animal selectAnimal(String name)
	{
		return (Animal) entityManager.createNativeQuery("Select * from petshop.Animal where(name = ?) LIMIT 1", Animal.class)
				.setParameter(1, name).getSingleResult();
				
	}
	
	/**
	 * A Delete function for Animal in the database
	 */
	public void deleteAnimal(Animal animal) {
		startTransaction();
		entityManager.createNativeQuery("Delete from petshop.Animal Where(idAnimal = ?)")
		.setParameter(1, animal.getIdAnimal())
		.executeUpdate();
		commitTransaction();
	}
	
	/**
	 * A Read function for Animal in the database
	 */
	public List<Animal> readAnimal() {
		//startTransaction();
		List<Animal> results = entityManager.createNativeQuery("Select * from petshop.Animal", Animal.class)
				.getResultList();
		for(Animal animal : results) {
			System.out.println("Animal: " + animal.getName() + " has ID " + animal.getIdAnimal());
		}
		//commitTransaction();
		return results;
	}
	
	/**
	 * A Create function for PersonalMedical in the database
	 */
	public void createPersonalMedical(Personalmedical personalMedical) {
		//startTransaction();
		entityManager.createNativeQuery("Insert into petshop.Personalmedical(idPersonalMedical, name)" 
		+ " Values (?, ?)")
		.setParameter(1, personalMedical.getIdPersonalMedical())
		.setParameter(2, personalMedical.getName())
		.executeUpdate();
		//commitTransaction();
	}
	
	/**
	 * An Update function for PersonalMedical in the database
	 */
	public void updatePersonalMedical(Personalmedical personalMedical) {
		//startTransaction();
		entityManager.createNativeQuery("Update petshop.Personalmedical Set name = ? Where(idPersonalMedical = ?)")
		.setParameter(1, personalMedical.getName())
		.setParameter(2, personalMedical.getIdPersonalMedical())
		.executeUpdate();
		//commitTransaction();
	}
	
	/**
	 * A Delete function for PersonalMedical in the database
	 */
	public void deletePersonalMedical(Personalmedical personalMedical) {
		//startTransaction();
		entityManager.createNativeQuery("Delete from petshop.Personalmedical Where(idPersonalMedical = ?)")
		.setParameter(1, personalMedical.getIdPersonalMedical())
		.executeUpdate();
		//commitTransaction();
	}
	
	/**
	 * A Read function for PersonalMedical in the database
	 */
	public List<Personalmedical> readPersonalMedical() {
		//startTransaction();
		List<Personalmedical> results = entityManager.createNativeQuery("Select * from petshop.Personalmedical", 
				Personalmedical.class).getResultList();
		for(Personalmedical personalMedical : results) {
			System.out.println("Personalmedical: " + personalMedical.getName() + " has ID " + personalMedical.getIdPersonalMedical());
		}
		//commitTransaction();
		return results;
	}
	
	/**
	 * A Create function for Programare in the database
	 */
	public void createProgramare(Programare programare) {
		//startTransaction();
		entityManager.createNativeQuery("Insert into petshop.Programare(idProgramare, data, idAnimal, idPersonalMedical)" 
		+ " Values (?, ?, ?, ?)")
		.setParameter(1, programare.getIdProgramare())
		.setParameter(2, programare.getData())
		.setParameter(3, programare.getPersonalmedical().getIdPersonalMedical())
		.setParameter(4, programare.getAnimal().getIdAnimal())
		.executeUpdate();
		//commitTransaction();
	}
	
	/**
	 * An Update function for Programare in the database
	 */
	public void updateProgramare(Programare programare) {
		//startTransaction();
		entityManager.createNativeQuery("Update petshop.Programare Set data = ?, idAnimal = ?, idPersonalMedical = ? Where(idProgramare = ?)")
		.setParameter(1, programare.getData())
		.setParameter(2, programare.getPersonalmedical().getIdPersonalMedical())
		.setParameter(3, programare.getAnimal().getIdAnimal())
		.setParameter(4, programare.getIdProgramare())
		.executeUpdate();
		//commitTransaction();
	}
	
	/**
	 * A Delete function for Programare in the database
	 */
	public void deleteProgramare(Programare programare) {
		//startTransaction();
		entityManager.createNativeQuery("Delete from petshop.Programare Where(idProgramare = ?)")
		.setParameter(1, programare.getIdProgramare())
		.executeUpdate();
		//commitTransaction();
	}
	
	/**
	 * A Read function for Programare in the database
	 */
	public List<Programare> readProgramare() {
		//startTransaction();
		List<Programare> results = entityManager.createNativeQuery("Select * from petshop.Programare", Programare.class)
				.getResultList();
		results.sort((m,n) -> (int) (m.getData().getTime() - n.getData().getTime()));
		for(Programare programare : results) {
			System.out.println("Programarea Nr. " + programare.getIdProgramare() + " at date " + programare.getData() 
					+ " is between Medic " + programare.getPersonalmedical().getName() + " and the Animal " 
					+ programare.getAnimal().getName());
		}
		//commitTransaction();
		return results;
	}
	
	public List<Programare> selectProgramari(String name)
	{
		Animal animal = selectAnimal(name);
		List<Programare> results = entityManager.createNativeQuery("Select * from petshop.Programare where(idAnimal = ?)", Programare.class)
				.setParameter(1,  animal.getIdAnimal())
				.getResultList();
		results.sort((m,n) -> (int) (m.getData().getTime() - n.getData().getTime()));
		return results;
	}
}