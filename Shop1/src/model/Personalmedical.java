package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the personalmedical database table.
 * 
 */
@Entity
@NamedQuery(name="Personalmedical.findAll", query="SELECT p FROM Personalmedical p")
public class Personalmedical implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int idPersonalMedical;

	private int age;

	private int greutate;

	private int inaltime;

	private String name;

	private String sex;

	//bi-directional many-to-one association to Programare
	@OneToMany(mappedBy="personalmedical")
	private List<Programare> programares;

	public Personalmedical() {
	}

	public int getIdPersonalMedical() {
		return this.idPersonalMedical;
	}

	public void setIdPersonalMedical(int idPersonalMedical) {
		this.idPersonalMedical = idPersonalMedical;
	}

	public int getAge() {
		return this.age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public int getGreutate() {
		return this.greutate;
	}

	public void setGreutate(int greutate) {
		this.greutate = greutate;
	}

	public int getInaltime() {
		return this.inaltime;
	}

	public void setInaltime(int inaltime) {
		this.inaltime = inaltime;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSex() {
		return this.sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public List<Programare> getProgramares() {
		return this.programares;
	}

	public void setProgramares(List<Programare> programares) {
		this.programares = programares;
	}

	public Programare addProgramare(Programare programare) {
		getProgramares().add(programare);
		programare.setPersonalmedical(this);

		return programare;
	}

	public Programare removeProgramare(Programare programare) {
		getProgramares().remove(programare);
		programare.setPersonalmedical(null);

		return programare;
	}

}