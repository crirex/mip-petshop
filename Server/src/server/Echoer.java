package server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class Echoer extends Thread {

	private Socket socket;

	public Echoer(Socket socket) {
		this.socket = socket;
	}

	//login
	//server+ client (pe server se verifica daca datele primite sunt corecte)
	// sa se poata loga in aplicatie
	@Override
	public void run() {
		try {
			BufferedReader input = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			PrintWriter output = new PrintWriter(socket.getOutputStream(), true);
			// true -> clean the buffer when is full
			String echoString = input.readLine();
			System.out.println("recieved from client");
			if(echoString.equals("username parola"))
			{
				output.println("good");					
			}
			else
			{
				output.println("bad");
			}
		} catch (IOException e) {

			e.printStackTrace();
		}
	}
}
