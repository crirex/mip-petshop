package util;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

public class LoggerAgent <T> {
	private T log;
	private PropertyChangeSupport support;

	public LoggerAgent() {
		support = new PropertyChangeSupport(this);
	}

	public void addPropertyChangeListener(PropertyChangeListener pcl) {
		support.addPropertyChangeListener(pcl);
	}

	public void removePropertyChangeListener(PropertyChangeListener pcl) {
		support.removePropertyChangeListener(pcl);
	}

	public T getLog() {
		return log;
	}

	public void setLog(T log) {
		support.firePropertyChange("log", this.log, log);
		this.log = log;
	}
}