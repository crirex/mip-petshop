package util;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

public class LoggerChannel <T> implements PropertyChangeListener {
	private T log;

	public T getLog() {
		return log;
	}

	public void setLog(T log) {
		this.log = log;
	}

	public void propertyChange(PropertyChangeEvent evt) {
		this.setLog((T)evt.getNewValue());
		System.out.println(log);

	}
}